import ssd1306
import machine
import neopixel
import time

# import touchbtns
import pocitani
import barvy
import kostka

import encoder

from touchbtns import pressed_buttons


def main_screen(disp):

    disp.fill_rect(0, 0, 128, 64, 0)
    disp.text(" >> VYBER SI << ", 0, 6)
    disp.text(" + - *    A | B ", 0, 31)
    disp.text("          C | D ", 0, 52)
    disp.hline(8, 24, 112, 1)
    disp.hline(78, 46, 40, 1)
    disp.vline(63, 24, 40, 1)
    disp.hline(8, 46, 56, 1)
    disp.fill_rect(26, 50, 2, 2, 1)
    disp.fill_rect(34, 50, 2, 2, 1)
    disp.fill_rect(30, 54, 2, 2, 1)
    disp.fill_rect(26, 58, 2, 2, 1)
    disp.fill_rect(34, 58, 2, 2, 1)

    disp.show()


def main():
    i2c = machine.I2C(sda=machine.Pin(5), scl=machine.Pin(4))

    disp = ssd1306.SSD1306_I2C(128, 64, i2c)
    buttons = [[machine.TouchPad(machine.Pin(32)), 540, 'A'],  # 538
               [machine.TouchPad(machine.Pin(33)), 500, 'B'],  # 495
               [machine.TouchPad(machine.Pin(27)), 550, 'C'],  # 547
               [machine.TouchPad(machine.Pin(14)), 550, 'D']]  # 346

    neopin = machine.Pin(18, machine.Pin.OUT)
    npix = neopixel.NeoPixel(neopin, 60)
    enc = encoder.IncRotEc11(0, 23, 22, 19, 2000, 1000)

    while True:

        for i in range(60):
            npix[i] = (0, 0, 0)
        npix.write()

        main_screen(disp)

        btns = pressed_buttons(buttons)

        if 0 in btns:
            pocitani.run(disp, enc, npix)
        elif 2 in btns:
            kostka.run(disp, enc, npix)
        elif 1 in btns or 3 in btns:
            bbtns = [buttons[0], buttons[1], buttons[3], buttons[2]]
            barvy.run(disp, bbtns, npix)

        time.sleep(1)


if __name__ == "__main__" and machine.reset_cause() != machine.SOFT_RESET:
    main()
