import time

def pressed_buttons(buttons, timeout=0):

    pressed_btns = []

    start = time.ticks_ms() # get millisecond counter
    while True:

        delta = time.ticks_diff(time.ticks_ms(), start)
        if timeout and delta > timeout*1000:
            break

        pressed_btns = []
        for i,b in enumerate(buttons):
            x = b[0].read()
            if x < b[1]:
                pressed_btns.append(i)

        if len(pressed_btns) > 0:
            break

    return pressed_btns


def pressed_button(buttons, timeout=0):

    pressed_btn = -1
    pressed = False
    shown = False

    start = time.ticks_us() # get millisecond counter
    while True:

        delta = time.ticks_diff(time.ticks_ms(), start)
        if timeout and delta > timeout:
            break

        if not pressed:
            for i,b in enumerate(buttons):
                x = b[0].read()
                if x < b[1]:
                    pressed_btn = i
                    pressed = True
        else:
            x = buttons[pressed_btn][0].read()
            if x >= buttons[pressed_btn][1]:
                break

    return pressed_btn


def pressed_button_resttime(buttons, timeout=0):

    pressed_btn = -1
    rest_time = 0
    pressed = False

    start = time.ticks_us() # get millisecond counter
    delta = 0
    while True:

        delta = time.ticks_diff(time.ticks_us(), start)
        if timeout and delta > timeout:
            break

        if not pressed:
            for i,b in enumerate(buttons):
                x = b[0].read()
                if x < b[1]:
                    pressed_btn = i
                    pressed = True
        else:
            x = buttons[pressed_btn][0].read()
            if x >= buttons[pressed_btn][1]:
                rest_time = timeout - delta
                break

    return pressed_btn, rest_time


def pressed_button_visible(disp, npix, buttons, show_btn, unshow_btn, offset=0, timeout=0):

    pressed_btn = -1
    pressed = False
    shown = False

    start = time.ticks_ms() # get millisecond counter
    while True:

        delta = time.ticks_diff(time.ticks_ms(), start)
        if timeout and delta > timeout*1000:
            break

        if not pressed:
            for i,b in enumerate(buttons):
                x = b[0].read()
                if x < b[1]:
                    pressed_btn = i
                    pressed = True
        else:

            if not shown:
                show_btn(disp, npix, pressed_btn, buttons[pressed_btn][2], offset)
                shown = True

            x = buttons[pressed_btn][0].read()
            if x >= buttons[pressed_btn][1]:
                break


    unshow_btn(disp, npix, 0, buttons[pressed_btn][2], offset)

    return pressed_btn

