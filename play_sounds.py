import time

from machine import Pin, PWM

buz_tim = PWM(Pin(25))
buz_tim.duty(0)

tones = {
    'x': 0,                                 #pomlka
    'C6': 1047, 'C#6': 1109, 'Db6': 1109,
    'D6': 1175, 'D#6': 1245, 'Eb6': 1245,
    'E6': 1319,
    'F6': 1397, 'F#6': 1480, 'Gb6': 1480,
    'G6': 1568, 'G#6': 1661, 'Ab6': 1661,
    'A6': 1760, 'A#6': 1865, 'Hb6': 1865,
    'H6': 1976,
    'C7': 2093, 'C#7': 2217, 'Db7': 2217,
    'D7': 2349, 'D#7': 2489, 'Eb7': 2489,
    'E7': 2637,
    'F7': 2794, 'F#7': 2960, 'Gb7': 2960,
    'G7': 3136, 'G#7': 3322, 'Ab7': 3322,
    'A7': 3520, 'A#7': 3729, 'Hb7': 3729,
    'H7': 3951,
    'C8': 4186
}


def play_tone(freq, msec):
    if freq > 0:
        buz_tim.freq(int(freq))
        buz_tim.duty(50)
    time.sleep_ms(int(msec * 0.9))
    buz_tim.duty(0)
    time.sleep_ms(int(msec * 0.1))


def play_melody(melody):
    for t in melody:
        play_tone( tones[t[0]], t[1])


def play_tone_start(freq):
    if freq > 0:
        buz_tim.freq(int(freq))
        buz_tim.duty(50)


def play_tone_stop():
    buz_tim.duty(0)

