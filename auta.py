import ustruct as struct
import machine
import ssd1306
import neopixel
import time
import framebuf

from play_sounds import play_tone_start, play_tone_stop, tones
from uos import urandom as random
from touchbtns import pressed_button_visible, pressed_button_resttime

konec = [
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0],
    [0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0],
    [0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0],
    [0,0,1,0,0,0,1,1,0,1,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,1,1,0,1,0,0],
    [0,0,0,1,0,0,1,1,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,1,0,0,1,1,1,0,0,0],
    [0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0],
    [0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0],
    [0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0],
    [0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0] ]


v_poradku = [
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0],
    [0,0,0,1,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,1,1,0,0,0],
    [0,0,1,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,1,1,1,0,0],
    [0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0],
    [0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0],
    [0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0],
    [0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0],
    [0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0],
    [0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0] ]

car_img = [
    [0,1,1,0],
    [1,1,1,1],
    [1,1,1,1],
    [0,1,1,0],
    [0,1,1,0],
    [1,1,1,1],
    [1,1,1,1] ]

npix_barvy = [ (5,3,0), (6,0,0), (0,6,0), (0,0,6) ]
tony = [ 'C6', 'E6', 'G6', 'H6']

offset = 51

class Road(framebuf.FrameBuffer):

    def __init__(self, buf, width, height, barrier_distance=15, door_width=10):

        self.buf = buf
        self.width = width
        self.height = height
        self.door_width = door_width
        self.road_pos = 0
        self.barrier_distance = barrier_distance

        super().__init__(self.buf, self.width, self.height, framebuf.MONO_HMSB)
        self.clear()


    def move(self, steps=1):

        self.scroll(0,1)
        self.hline(0,0, self.width, 0)

        if not self.road_pos:
            self.mk_barrier()

        self.road_pos = (self.road_pos + 1) % self.barrier_distance

    def clear(self):
        self.fill(0)

    def mk_barrier(self):
        x = 0
        
        while x <= self.width-self.door_width:
            wall_len = struct.unpack(">H", random(2))[0] % ((self.width-self.door_width)//2)

            if x+wall_len > self.width-self.door_width and x+wall_len < self.width:
                wall_len = self.width - self.door_width - x #adjust size of last doors
            elif x+wall_len > self.width:
                wall_len = self.width - x

            self.hline(x,0, wall_len, 1)
            x += self.door_width + wall_len


    #fbuf.blit is causing segfault, so we will do things in old, slow way...
    def put_on(self, fbuf, pos_x, pos_y, transp=-1):
        for y in range(self.height):
            for x in range(self.width):
                if transp != self.pixel(x,y):
                    fbuf.pixel(pos_x+x, pos_y+y, self.pixel(x,y))


    def collision(self, car):
        coll = False

        for cy in range(car.height):
            for cx in range(car.width):
                if self.pixel(car.pos_x+cx, car.pos_y+cy) and car.pixel(cx,cy):
                    coll = True
                    break

        return coll


class Car(framebuf.FrameBuffer):

    def __init__(self, buf, car_pix, range_x, range_y):

        self.width = len(car_pix[0])
        self.height = len(car_pix)
        self.buf = buf
        self.range_x = range_x
        self.range_y = range_y
    
        self.pos_x = (range_x[0]+range_x[1])//2
        self.pos_y = (range_y[0]+range_y[1])//2

        super().__init__(self.buf, self.width, self.height, framebuf.MONO_HMSB)
        self.fill(0)

        for y in range(len(car_pix)):
            for x in range(len(car_pix[y])):
                self.pixel(x,y, car_pix[y][x])

    #fbuf.blit is causing segfault, so we will do things in old, slow way...
    def put_on(self, fbuf, pos_x, pos_y, transp=-1):
        for y in range(self.height):
            for x in range(self.width):
                if transp != self.pixel(x,y):
                    fbuf.pixel(pos_x+x, pos_y+y, self.pixel(x,y))

    def move_x(self, x_move):
        self.pos_x = max(self.range_x[0], self.pos_x+x_move) if x_move < 0 else min(self.range_x[1], self.pos_x+x_move)

    def move_y(self, y_move):
        self.pos_y = max(self.range_y[0], self.pos_y+y_move) if y_move < 0 else min(self.range_y[1], self.pos_y+y_move)



def void_show_btn(disp, npix, n, bn, offset):
    pass


def run(disp, buttons, neopix):

    for i in range(60):
        neopix[i] = (0,0,0)
    neopix.write()

    disp.fill_rect(0,0, 128, 64, 0)
    #disp.text(" JSI PRIPRAVEN? ", 0, 6)
    #disp.text("Zmackni tlacitko", 0, 32)
    #disp.text("   ~~=<<>>=~~   ", 0, 44)
    #disp.show()
    #time.sleep_ms(1500)
    #_ = pressed_button_visible(disp, neopix, buttons, void_show_btn, void_show_btn, offset=offset)
    #time.sleep_ms(500)

    counter = 0

    lives = 4

    buf_road = bytearray(512)    
    road = Road(buf_road,  64, 64)
    
    buf_car = bytearray(2)
    car = Car(buf_car, car_img, (0,60), (59,59))

    while lives:
      
        if not counter:
            road.move()
            road.put_on(disp, 32, 0)
            car.put_on(disp, 32+car.pos_x, 0+car.pos_y, transp=0)
            if road.collision(car):
                print("kolize")
                lives -= 1

            disp.show()

        btn, rest_time = pressed_button_resttime(buttons, timeout=5000)
        time.sleep_us(rest_time)

        if btn != -1:
            cm = 0
            if btn == 2:
                cm = -1
            elif btn == 3:
                cm = 1
            car.move_x(cm)
            car.put_on(disp, 32+car.pos_x, 0+car.pos_y, 0)
            disp.show()

        counter = (counter + 1) % 50



def main():
    i2c = machine.I2C(sda=machine.Pin(5), scl=machine.Pin(4))

    disp = ssd1306.SSD1306_I2C(128, 64, i2c)

    buttons = [ [ machine.TouchPad(machine.Pin(32)), 450, 'A' ],
                [ machine.TouchPad(machine.Pin(33)), 450, 'B' ],
                [ machine.TouchPad(machine.Pin(27)), 450, 'C' ],
                [ machine.TouchPad(machine.Pin(02)), 360, 'D' ],
              ]

    neopin = machine.Pin(18, machine.Pin.OUT)
    npix = neopixel.NeoPixel(neopin, 60)

    run(disp, buttons, npix)


if __name__ == "__main__" and machine.reset_cause() != machine.SOFT_RESET:
    main()

