"""
code for handling encoders attched to ESP32 16-Bit pulse counting units

Counting sequence and over/under-run behaviour of hardware counters must be considered when using encoder algorithms
Up/down:    0, 1, 2, 3, 2, 1, 0, -1, -2, -3, -2, -1, 0, 1, 2, 3, 4, ...
Up:	        0, 1, 2, 3, ..., 32765, 32766, 0, 1, 2, 3, ...
Down:       0, -1, -2, -3, ..., -32765, -32766, 0, -1, -2, -3, ...

Author: sengp
Date: 2020-02-03

"""

from machine import Pin
from machine import DEC

switch_pressed = 0


def switch_callback(p):
    global switch_pressed

    switch_pressed += 1



class IncRotEc11:  # create class IncRotEc11
    """
    class for handling incremental rotary encoders including push momentary switch
        - hardware counter based on ESP32 pulse counting unit, 16-Bit signed
        - quadrature decoder based on ESP32 pulse counting unit
        - debounce functionality based on ESP32 pulse counting unit not implemented (filter missing at class DEC)
        - no need for timer interrupt or polling
    Code implemented and based on these sources:
        https://people.eecs.berkeley.edu/~boser/courses/49_sp_2019/N_gpio.html#_quadrature_decoder
        https://github.com/bboser/MicroPython_ESP32_psRAM_LoBo/blob/quad_decoder/MicroPython_BUILD/components/micropython/esp32/machine_dec.c

    ATTENTION: Prevent position error by avoiding over/under-run of hardware counter.
               Call of method "setpos" resets hardware counter value to 0. Call of method "setpos" at appropriate location in user program is a
               suitable practice to prevent hardware-counter overrun (use actual position as argument). Use method "getcnt" to obtain
               information about counter values span from becomming critical (over/under-run), or to implement fastgear functionality

    optimized for encoders like e.g. ALPS EC11 or BOURNS PEC11L series:
        - e.g. 30 detents/revolution and channel A and B each generating 15 pulses/revolution leads to 60 impulses/revolution
          Hint: on a decoder described above, impulses must be divided by 2 to result in one impulse/detent
        - channel A -> bit0, channel B -> bit1
        - cyclic AB values are (CW: left to right, CCW: right to left): ...,0,1,3,2,0,1,3,2,0,1,3,2,0,1,...
        - mechanical detents at AB value 0 and 3
        - suggested circuit to connect rotary encoder including switch:
            - A/B and switch common connected to GND
            - A/B connected via 10K pullup to VCC
            - A/B channel output low pass filtering via 10K/10nF, see BOURNS PEC11L datasheet
              Hint: low pass filtering is recommended to prevent glitches at counter input
            - do not enable internal pullup for A/B channel at MCU input when low pass filter is assembled
            - at switch input internal pullup at MCU input enabled
        - when turning right position increments by 1 in range of 0...MaxPos
        - when turning left position decrements by 1 in range of 0...MaxPos

    __init__ arguments
        IncRotEc11(Unit, PinA, PinB, PinSwitch, MaxPos, InitPos, Filter)
            Unit
                number of pulse count unit
                    0 ... 7
            PinA
                pin number channel A
            PinB
                pin number channel B
            PinSwitch
                pin number switch
            MaxPos
                position maximum value
            InitPos
                position init value
                    0 ... MaxPos

    methods
        .getpos()
            RetVal
                0 ... MaxPos
        .setpos(position)
            position
                0 ... MaxPos
        .getcnt()
            hardware counter value
                -32765 ... 32765

    variables
        .switch.value()
            0 = pressed
            1 = not pressed
        .position
            0 ... MaxPos
    """

    def __init__(self, Unit, PinA, PinB, PinSwitch, MaxPos, InitPos):  # method is automatically called when new instance of class is created
        self.pinA = Pin(PinA)
        self.pinB = Pin(PinB)
        self.pinA.irq(handler=self.pinCB, trigger=Pin.IRQ_FALLING | Pin.IRQ_RISING)
        self.pinB.irq(handler=self.pinCB, trigger=Pin.IRQ_FALLING | Pin.IRQ_RISING)
        self.dec = DEC(Unit, self.pinA, self.pinB)
        self.switch = Pin(PinSwitch, Pin.IN)  # encoder pushbutton, PinSwitch, enable internal pull-up resistor
        self.MaxPos = MaxPos
        self.offset = InitPos
        self.count = 0
        self.position = self.calcpos()

    def pinCB(self, pin):
        print(pin, self.dec.count())

    def calcpos(self,):
        # arguments in position calculation can be positive or negative. Following line of code is valid when using Python,
        # cause Python uses the "mathematical variant" of the modulo operator. This code will fail when it is ported to e.g.
        # C, C++ or Java, cause these languages implement the "symmetrical variant" of the modulo operator, leading to
        # wrong results (in this algorithm) on negative arguments.
        return ((self.count + self.offset) % (self.MaxPos + 1))

    def getpos(self,):
        self.count = (self.dec.count() >> 1)  # read counter and divide counter value by 2 to get one impulse/detent
        self.position = self.calcpos()
        return self.position


    def getpos_switch(self,):

        global switch_pressed

        self.switch.irq(trigger=Pin.IRQ_FALLING, handler=switch_callback)

        old = -1
        while switch_pressed == 0:
            pos = self.getpos()
            if pos != old:
                yield pos
                old = pos

        self.switch.irq(trigger=0, handler=None)
        switch_pressed = 0



    def setpos(self, pos):
        if (pos >= 0) and (pos <= self.MaxPos):
            self.dec.clear()
            self.offset = pos
            self.count = 0
            self.position = self.calcpos()

    def getcnt(self,):
        return self.count
