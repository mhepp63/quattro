import ssd1306
import machine
import ustruct as struct
import time
import neopixel

# from play_sounds import play_melody, play_tone, tones

from uos import urandom as random
# from touchbtns import pressed_button

import encoder

kostky = [[4, 6, 8, 10, 12, 24],
          [0, 4, 6, 8, 10, 12, 24]]

exitus = 0


def exitCallback(pin):
    global exitus
    exitus = 1


def generuj_cislo(rozsah, disp, neopixel):

    disp.fill_rect(0, 0, 128, 64, 0)
    disp.fill_rect(48, 32, 32, 32, 1)
    disp.fill_rect(52, 36, 24, 24, 0)

    txt = "Kostk{}: {:2d}{} {}".format("y" if rozsah[1] else "a", rozsah[0],
                                       "," if rozsah[1] else " ",
                                       rozsah[1] if rozsah[1] else " ")
    disp.text(txt, 0, 1)

    cislo1 = struct.unpack(">Q", random(8))[0] % rozsah[0] + 1
    cislo2 = (struct.unpack(">Q", random(8))[0] % rozsah[1] +
              1) if rozsah[1] != 0 else 0

    if rozsah[1]:
        txt = "Cisla: ?? a ??"
        disp.text(txt, 0, 14)

    disp.show()

    cislo = cislo1 + cislo2

    posuv = 60/cislo

    for i in range(2):
        for p in range(60):
            neopixel[p % 60] = (2, 2, 2)
            neopixel[(p + 59) % 60] = (0, 0, 0)
            neopixel.write()
            time.sleep_ms(10)
    neopixel[59] = (0, 0, 0)

    for p in range(60):
        neopixel[p % 60] = (2, 2, 2)
        b = (0, 0, 0)
        if int(p % posuv):
            pass
        else:
            b = (1, 0, 3)
        neopixel[(p + 59) % 60] = b
        neopixel.write()
        time.sleep_ms(10)
    neopixel[59] = (1, 0, 3)
    neopixel.write()

    if rozsah[1]:
        txt = "Cisla: {:2d} a {:2d}".format(cislo1, cislo2)
        disp.fill_rect(0, 14, 128, 8, 0)
        disp.text(txt, 0, 14)

    disp.text("{:2d}".format(cislo), 58, 43)
    disp.show()


def run(disp, encoder, neopix):

    global exitus

    exitbtn = machine.Pin(0)
    exitbtn.irq(trigger=machine.Pin.IRQ_FALLING, handler=exitCallback)

    for i in range(60):
        neopix[i] = (0, 0, 0)
    neopix.write()

    encoder.setpos(0)
    rozsah = [0, 0]

    disp.fill_rect(0, 0, 128, 64, 0)
    disp.text("  Vyber kostky  ", 0, 24)
    disp.text("   ~~=<<>>=~~   ", 0, 36)
    disp.show()

    for j in [0, 1]:
        encoder.setpos(int(((encoder.MaxPos / 2) // len(kostky[j])) *
                           len(kostky[j])))
        k = "B" if j else "A"
        for i in encoder.getpos_switch():
            print(j, i, kostky[j][i % len(kostky[j])])
            rozsah[j] = kostky[j][i % len(kostky[j])]
            disp.fill_rect(0+j*64, 48, 64, 16, 0)
            disp.text("{}: {}".format(k, kostky[j][i % len(kostky[j])]),
                      12+j*64, 48)
            disp.show()

        time.sleep_ms(500)

    while True:
        generuj_cislo(rozsah, disp, neopix)

        for x in encoder.getpos_switch():
            if exitus:
                break
        if exitus:
            exitus = 0
            break

    exitbtn.irq(trigger=0, handler=None)


def main():
    i2c = machine.I2C(sda=machine.Pin(5), scl=machine.Pin(4))

    disp = ssd1306.SSD1306_I2C(128, 64, i2c)

    # buttons = [[machine.TouchPad(machine.Pin(32)), 450, 'A'],
    #            [machine.TouchPad(machine.Pin(33)), 450, 'B'],
    #            [machine.TouchPad(machine.Pin(27)), 450, 'C'],
    #            [machine.TouchPad(machine.Pin(14)), 550, 'D']]

    neopin = machine.Pin(18, machine.Pin.OUT)

    npix = neopixel.NeoPixel(neopin, 60)

    enc = encoder.IncRotEc11(0, 23, 22, 19, 2000, 1000)

    run(disp, enc, npix)


if __name__ == "__main__" and machine.reset_cause() != machine.SOFT_RESET:
    main()
